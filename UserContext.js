import { createContext, useContext, useState, useEffect, useCallback } from 'react';
import { useNavigation } from '@react-navigation/native';
import { API_URL } from '@env';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const UserContext = createContext();
export const UserProvider =  ({children}) => {
    const navigation = useNavigation();
    const [user, setUser] = useState(null);
    const [isRegisterLoading, setIsRegisterLoading] = useState(false);
    const [registerError, setRegisterError] = useState(null);
    const [loginError, setLoginError] = useState(null);
    const [isLoginLoading, setIsLoginLoading] = useState(false);
    const [registerInfo, setRegisterInfo] = useState({
        name: '',
        email: '',
        password: ''
    })

    const [loginInfo, setLoginInfo] = useState({
        name: '',
        email: '',
        password: ''
    })

    const updateRegisterInfo = useCallback(info => {
        setRegisterInfo(info);
    }, [])

    const updateLoginInfo = useCallback(info => {
        setLoginInfo(info);
    }, [])

    const registerUser = useCallback(async(e)=> {
        e.preventDefault();
        setIsRegisterLoading(true);
        await fetch(`${API_URL}/users/register`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(registerInfo)
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data.token){
                setRegisterInfo({
                    name: '',
                    email: '',
                    password: ''
                });
                navigation.navigate('Login');
            }
            else{
                setRegisterError(data)
            }
        }).catch(err => {
            setRegisterError(err);
        })
        .finally(() => {
            setIsRegisterLoading(false);
        })
    }, [registerInfo])


    const loginUser = useCallback(async(e) => {
        console.log(API_URL)
        e.preventDefault();
        setIsLoginLoading(true);
        await fetch(`${API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(loginInfo)
        })
        .then(result => result.json())
        .then(data => {

            if(data._id){
                AsyncStorage.setItem('USER', JSON.stringify(data));
                setUser(data);
                console.log('Login details', data)
                setLoginInfo('');
                setLoginError(null);
                navigation.navigate('Todo');
            }
            else{
                console.log(data);
                setLoginError(data);
            }
        })
        .catch(err => {
            console.error(err)
            setLoginError('Error with Login');
        })
        .finally(() => {
            setIsLoginLoading(false);
        })

    }, [loginInfo])

    return(
        <UserContext.Provider
            value={{
                user,
                registerInfo,
                updateRegisterInfo,
                registerUser,
                registerError,
                isRegisterLoading,
                loginInfo,
                updateLoginInfo,
                loginUser,
                loginError,
                isLoginLoading
            }}
        >
        {children}
        </UserContext.Provider>
    )

}