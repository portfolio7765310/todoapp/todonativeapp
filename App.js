import * as React from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Login from './pages/Login';
import Register from './pages/Register';
import Todo from './pages/Todo';

import { UserContext, UserProvider } from './UserContext';

const Stack = createStackNavigator();
export default function App() {

  
  return (
    
    <NavigationContainer>
      <UserProvider>
      <Stack.Navigator initialRouterName="Login">
        <Stack.Screen name="Login" component={Login}/>
        <Stack.Screen name="Register" component={Register}/>
        <Stack.Screen name="Todo" component={Todo} />
      </Stack.Navigator>
      <StatusBar style="auto" />
      </UserProvider>
    </NavigationContainer>
    
    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'gray',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
