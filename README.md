  <h2>Cloning Project</h2>
  <p>This project was created with React native expo.</p>
  
  <h3>To run the project after cloning in your computer;</h3>
  <p><strong>npm install</strong></p>
  <p>Go to project directory, install and update the packages in this project.</p>

  <p><strong>npx expo start</strong></p>
  <p>To run the application.</p>

  <h3>Connect your android device</h3>
  <p>Download <a href="https://expo.dev/client" target="_blank">Expo Go</a> in your android, open and scan the QR code from the terminal.</p>


  <h3>Connect the API</h3>
  <p>Clone the API from this <a href="https://gitlab.com/portfolio7765310/todoapp/server" target="_blank">link</a>.</p>
