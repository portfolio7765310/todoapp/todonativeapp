import {TextInput, SafeAreaView, StyleSheet, Text, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Todo from './Todo';
import React, { useState, useEffect, useContext } from 'react';
import { API_URL } from '@env';

import { UserContext } from '../UserContext';

export default function Login ({navigation}){
    
    // const [user, setUser] = useState(null);
    // const [email, setEmail] = useState('');
    // const [password, setPassword] = useState('');
    // const [isLoginLoading, setIsLoginLoading] = useState(false);
    // const [error, setError] = useState(null);

    // const loginUser = async(e) => {
    //     console.log(API_URL)
    //     e.preventDefault();
    //     setIsLoginLoading(true);
    //     await fetch(`${API_URL}/users/login`, {
    //         method: 'POST',
    //         headers: {
    //             'Content-Type': 'application/json'
    //         },
    //         body: JSON.stringify({
    //             email: email,
    //             password: password
    //         })
    //     })
    //     .then(result => result.json())
    //     .then(data => {

    //         if(data._id){
    //             AsyncStorage.setItem('USER', JSON.stringify(data));
    //             console.log('Login details', data)
    //             setUser(data)
    //             setError(null)
    //             setEmail('');
    //             setPassword('');
    //             navigation.navigate('Todo');
    //         }
    //         else{
    //             console.log(data);
    //             setError(data);
    //         }
    //     })
    //     .catch(err => {
    //         console.error(err)
    //         setError('Error with Login');
    //     })
    //     .finally(() => {
    //         setIsLoginLoading(false);
    //     })

    // }
    // const showUser = async(e) => {
    //     const value = await AsyncStorage.getItem('USER');
    //     if(value !== null) {
    //         console.log(value);
    //     }
    //     else{
    //         console.error(value);
    //     }
    // }

    const {loginInfo, updateLoginInfo, loginUser, loginError, isLoginLoading} = useContext(UserContext);

    return(
        <SafeAreaView style={[{paddingTop: '50%'}]}>
            
            <TextInput 
                type="email" 
                placeholder='Email' 
                onChangeText={text => updateLoginInfo({...loginInfo, email: text})}
                value={loginInfo.email}
                style={styles.input} 
                required
            />
            <TextInput 
                type="password" 
                placeholder='Password' 
                onChangeText={text => updateLoginInfo({...loginInfo, password: text})}
                value={loginInfo.password}
                style={styles.input} 
                required
            />
            <TouchableOpacity style={styles.button} type="submit" onPress={e => loginUser(e)}>
                <Text style={styles.button} >{isLoginLoading? 'Signing in...' : 'Login'}</Text>
            </TouchableOpacity>
            
            <TouchableOpacity onPress={() => navigation.navigate('Register')} style={[styles.row, {alignContent: 'center', marginTop: 15}]}>
                
                <Text style={[styles.col, styles.text]}>Sign-up here</Text>
            </TouchableOpacity>
            {(loginError)?<Text style={styles.error}>{loginError}</Text>: null}
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    input: {
      height: 40,
      margin: 12,
      borderWidth: 1,
      width: 250,
      borderRadius: 10,
      marginRight: 'auto',
      marginLeft: 'auto',
      paddingLeft: 10
    },
    button:{
        borderRadius: 50,
        backgroundColor: 'black',
        color: 'white',
        textAlign: 'center',
        width: 100,
        paddingTop: 5,
        paddingBottom: 5,
        marginLeft: 'auto',
        marginRight: 'auto'
    },
    text: {
        fontWeight: 'bold'
    },
    row: {
        flexDirection: 'row'        
      },
    col: {
        flex: 1,
        height: 20,
        borderRadius: 5,
        textAlign: 'center',
        marginTop: 10
    },
    error: {
        borderRadius: 5,
        backgroundColor: '#FF2315',
        color: 'white',
        textAlign: 'center',
        width: 130,
        marginTop: 7,
        padding: 10,
        marginLeft: 'auto',
        marginRight: 'auto'
    }
  });