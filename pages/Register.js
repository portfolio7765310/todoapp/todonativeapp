import { useState, useContext } from 'react';
import {TextInput, SafeAreaView, StyleSheet, View, Text, Alert, TouchableOpacity } from 'react-native';
import { API_URL } from '@env';
import { UserContext } from '../UserContext';

export default function Register ({navigation}){
    
    const {registerInfo, updateRegisterInfo, registerUser, registerError, isRegisterLoading} = useContext(UserContext);

    return(
        <SafeAreaView style={[{paddingTop: '50%'}]}>
            <TextInput 
                type="text" 
                placeholder='Name' 
                onChange={e => updateRegisterInfo({...registerInfo, name: e.nativeEvent.text})}
                value={registerInfo.name}
                style={styles.input} 
                required
            />
            <TextInput 
                type="email" 
                placeholder='Email' 
                onChange={e => updateRegisterInfo({...registerInfo, email: e.nativeEvent.text})}
                value={registerInfo.email}
                style={styles.input} 
                required
            />
            <TextInput 
                type="password" 
                placeholder='Password' 
                onChange={e => updateRegisterInfo({...registerInfo, password: e.nativeEvent.text})}
                value={registerInfo.password}
                style={styles.input} 
                required
            />
            <TouchableOpacity style={styles.button}>
                <Text style={styles.button} type="submit" onPress={registerUser}>{isRegisterLoading? 'Creating account':'Register'}</Text>
            </TouchableOpacity>
            
            <TouchableOpacity onPress={() => navigation.navigate('Login')} style={[styles.row, {alignContent: 'center', marginTop: 15}]}>
                <Text style={[styles.col, styles.text]}>Sign-in here</Text>
            </TouchableOpacity>
            {(registerError)?<Text style={styles.error}>{registerError}</Text>: null}
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    input: {
      height: 40,
      margin: 12,
      borderWidth: 1,
      width: 250,
      borderRadius: 10,
      marginRight: 'auto',
      marginLeft: 'auto',
      paddingLeft: 10
    },
    button:{
        borderRadius: 50,
        backgroundColor: 'black',
        color: 'white',
        textAlign: 'center',
        width: 130,
        paddingTop: 5,
        paddingBottom: 5,
        marginLeft: 'auto',
        marginRight: 'auto'
    },
    text: {
        fontWeight: 'bold'
    },
    row: {
        flexDirection: 'row'        
      },
    col: {
        flex: 1,
        height: 20,
        borderRadius: 5,
        textAlign: 'center',
        marginTop: 10
    },
    error: {
        borderRadius: 5,
        backgroundColor: '#FF2315',
        color: 'white',
        textAlign: 'center',
        width: 'auto',
        marginTop: 7,
        padding: 10,
        marginLeft: 'auto',
        marginRight: 'auto'
    }
  });