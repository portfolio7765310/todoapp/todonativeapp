import { SafeAreaView, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity } from 'react-native';
import { useState, useEffect, useContext } from 'react';
import { API_URL } from '@env';
import  AsyncStorage  from '@react-native-async-storage/async-storage';
import CircularJSON from 'circular-json';
import { NavigationContainer, useNavigation } from '@react-navigation/native'
import { UserContext } from '../UserContext';
import Icon from '@mdi/react';
import { mdiDelete } from '@mdi/js'

export default function Todo (){

    const { navigation } = useNavigation();

    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [updatedTask, setUpdatedTask] = useState('');
    const [isError,setIsError] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const [userData, setUserData] = useState(null);
    const [taskList, setTaskList] = useState('');
    const [isTaskUpdated, setIsTaskUpdated] = useState(false);
    const [isEditing, setIsEditing] = useState(false);
    const [editLoading, setEditLoading] = useState(false);
    const [editingTask, setIsEditingTask] = useState(null);

    // const []

    const { user } = useContext(UserContext);

    const addTask = async(e) => {
        e.preventDefault();
        setIsLoading(true);
        
        await fetch(`${API_URL}/tasks/create`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${user?.token}`
            },
            body: CircularJSON.stringify({
                title: title,
                description: description
            })
        })
        .then(res => res.json())
        .then(data => {
            // console.log("ADDTASK", data);
            if(data !== undefined || null){
                setUpdatedTask(data);
                console.log('CREATED',data);
                
            }
            else{
                setError(error);
            }
        })
        .catch(error => {
            // console.log(error);
            setIsError(error);
        })
        .finally(() => {
            setIsLoading(false);
        })
    }
    
    
    const displayTasks = async()=>{

        const response = await fetch(`${API_URL}/users/find/${user?._id}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user?.token}`
            }
        })
        const data = await response.json();
        const tasks = data.map(task => {
          return (
            <SafeAreaView key={task._id} style={styles.mappedTask}>
                <TouchableOpacity onPress={() => editField(task)}>
                    <Text style={{fontWeight: 'bold', fontSize: 20}}>{task.title}</Text>
                    <Text>{task.description}</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.deleteButton} onPress={() => deleteTask(task)}>
                    <Text style={styles.deleteText}>Delete</Text>
                </TouchableOpacity>
            </SafeAreaView>
          );
        });
        setTaskList(tasks);
        // console.log(taskList)
        
    }

    const editField = async (task) => {
        // console.log("TASK ID",task)
        setIsEditing(true);
        setTitle(task.title);
        setDescription(task.description);
        setIsEditingTask(task);
    }
        
    const editTask = async(e) => {
        e.preventDefault();
        setEditLoading(true);
        await fetch(`${API_URL}/tasks/edit/${editingTask._id}`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user?.token}`
            },
            body: JSON.stringify({
                title: title,
                description: description
            })
        })
        .then(res => json())
        .then(data => {
            console.log('EDITED',data)
            if(data._id){
                setUpdatedTask(data);
                setTitle('');
                setDescription('');
                displayTasks();
            }
            else{
                setIsError(data)
            }
            
        })
        .catch(err => {
            setIsError(err)
        })
        .finally(() =>{ 
            setTitle('');
            setDescription('');
            displayTasks();
            setIsEditing(false)
            setEditLoading(false);
        })
    }


    const deleteTask = async(task) => {

        await fetch(`${API_URL}/tasks/delete/${task._id}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user.token}`
            },
            body: JSON.stringify(task)
        })
        .then(res => res.json())
        .then(data => {
            console.log('DELETED',data)
            if(data._id){
                displayTasks();
            }
            if(task._id === updatedTask._id){
                setTitle('');
                setDescription('');
                setIsEditing(false);
            }
            else{
                setIsError(data);
            }
        })
        .catch(err => {
            setIsError(err);
        })
    }

    useEffect(() => {
        displayTasks();
    }, [updatedTask])


    return(

        <SafeAreaView style={styles.container}>
            <Text style={[{fontSize: 50, fontWeight: 'bold'}]}>To Do List</Text>
            {isEditing?
            <>
            <TextInput 
                placeholder="Update task title" 
                type="text" 
                onChangeText={text => setTitle(text)}
                value={title}
                style={styles.input}
            />
            <TextInput 
                placeholder="Update description" 
                type="text" 
                onChangeText={text => setDescription(text)}
                value={description}
                style={styles.input}
            />
            <TouchableOpacity style={styles.button} type="submit" onPress={editTask}>
                <Text style={styles.text}>{editLoading? 'Updating' : 'Edit task'}</Text>
            </TouchableOpacity>
            </>
            :
            <>
            <TextInput 
                placeholder="What is your task today?" 
                type="text" 
                onChangeText={text => setTitle(text)}
                value={title}
                style={styles.input}
            />
            <TextInput 
                placeholder="Add a description" 
                type="text" 
                onChangeText={text => setDescription(text)}
                value={description}
                style={styles.input}
            />
            <TouchableOpacity style={styles.button} type="submit" onPress={addTask}>
                <Text style={styles.text}>{isLoading? 'Creating' : 'Add task'}</Text>
            </TouchableOpacity>
            </>
            }
            <ScrollView style={styles.task}>{taskList}</ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        paddingTop: '20%'
    },
    input: {
        width: '80%',
        borderWidth: 1,
        margin: 5,
        padding: 5,
        borderRadius: 7,
        paddingLeft: 20
    },
    button: {
        borderWidth: 1,
        borderRadius: 50,
        padding: 10,
        backgroundColor: 'black'
    },
    text: {
        color: 'white',
        textAlign: 'center',
        width: 100
    },
    task:{
        width: "80%",
        padding: 15,
        display: 'flex',
        flexDirection: 'column'
    },
    mappedTask:{
        borderWidth: 0.5,
        borderRadius: 5,
        margin: 5,
        padding: 5,
    },
    deleteText: {
        borderWidth: 0.5,
        backgroundColor: 'red',
        color: 'white',
        textAlign: 'center',
        width: 100
    },
    deleteButton:{
        alignContent: 'center',
        justifyContent: 'center',
        marginLeft: 'auto'
    }
})